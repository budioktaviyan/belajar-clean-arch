package id.kotlin.arch.data.movie

import io.reactivex.Single

class MovieFactory(private val datasource: MovieDatasource) {

  fun discoverMovie(): Single<MovieResponse> = datasource.discoverMovie()
}