package id.kotlin.arch.data.movie

import id.kotlin.arch.BuildConfig
import id.kotlin.arch.core.Config
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieDatasource {

  @GET("${Config.API_VERSION}/discover/movie")
  fun discoverMovie(
      @Query("api_key")
      apiKey: String = BuildConfig.API_KEY
  ): Single<MovieResponse>
}