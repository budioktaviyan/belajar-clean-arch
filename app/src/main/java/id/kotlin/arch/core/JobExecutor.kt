package id.kotlin.arch.core

import java.util.concurrent.Executor
import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.ThreadFactory
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

interface ThreadExecutor : Executor

class JobExecutor : ThreadExecutor {

  private val threadPoolExecutor: ThreadPoolExecutor = ThreadPoolExecutor(
      3,
      5,
      10,
      TimeUnit.SECONDS,
      LinkedBlockingDeque(),
      JobThreadFactory()
  )

  override fun execute(command: Runnable?) {
    threadPoolExecutor.execute(command)
  }
}

class JobThreadFactory(private val counter: Int = 0) : ThreadFactory {

  override fun newThread(runnable: Runnable?): Thread = Thread(
      runnable, "android_${counter.inc()}"
  )
}