package id.kotlin.arch.core

import id.kotlin.arch.data.common.Response
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import retrofit2.HttpException

@UseExperimental(UnstableDefault::class)
internal fun Throwable.response() =
    (this as? HttpException)?.response()?.errorBody()
        .let { response ->
          val serializer = Response.serializer()
          val default = Json.stringify(serializer, Response())
          val source = response?.source()?.readUtf8() ?: default
          Json.parse(serializer, source)
        }