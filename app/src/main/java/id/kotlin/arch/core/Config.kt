package id.kotlin.arch.core

import id.kotlin.arch.BuildConfig

object Config {

  const val API_VERSION = "/3"
  const val IMAGE_PATH = "${BuildConfig.IMAGE_URL}/t/p/original"
}