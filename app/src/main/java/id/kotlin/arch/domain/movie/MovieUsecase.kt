package id.kotlin.arch.domain.movie

import id.kotlin.arch.core.JobExecutor
import id.kotlin.arch.core.UIThread
import id.kotlin.arch.domain.common.Usecase
import io.reactivex.Single

class MovieUsecase(
    private val repository: MovieRepository
) : Usecase<MovieEntity, Any>(
    JobExecutor(), UIThread()
) {

  override fun buildUsecaseObservable(params: Any): Single<MovieEntity> =
      repository.discoverMovie()
}