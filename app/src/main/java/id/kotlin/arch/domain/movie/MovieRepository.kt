package id.kotlin.arch.domain.movie

import id.kotlin.arch.core.Config
import id.kotlin.arch.data.movie.MovieFactory
import io.reactivex.Single

class MovieRepository(private val factory: MovieFactory) {

  fun discoverMovie(): Single<MovieEntity> =
      factory.discoverMovie().map { response ->
        val results = response.results.map { result ->
          val title = result.title
          val image = "${Config.IMAGE_PATH}/${result.posterPath}"
          Result(title = title, image = image)
        }
        MovieEntity(results)
      }
}