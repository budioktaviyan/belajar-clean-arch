package id.kotlin.arch.domain.movie

data class MovieEntity(val results: List<Result>)

data class Result(
    val image: String,
    val title: String
)