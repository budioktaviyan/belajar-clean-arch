package id.kotlin.arch.presentation.movie

import id.kotlin.arch.domain.movie.Result
import id.kotlin.arch.presentation.common.MVPView

interface MovieView : MVPView {

  fun onSuccess(entity: List<Result>)
  fun onError(error: String)
}