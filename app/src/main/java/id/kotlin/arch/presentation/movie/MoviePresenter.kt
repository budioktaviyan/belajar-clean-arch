package id.kotlin.arch.presentation.movie

import id.kotlin.arch.core.Network
import id.kotlin.arch.core.response
import id.kotlin.arch.data.movie.MovieDatasource
import id.kotlin.arch.data.movie.MovieFactory
import id.kotlin.arch.domain.common.DefaultObserver
import id.kotlin.arch.domain.movie.MovieEntity
import id.kotlin.arch.domain.movie.MovieRepository
import id.kotlin.arch.domain.movie.MovieUsecase
import id.kotlin.arch.presentation.common.MVPPresenter

class MoviePresenter : MVPPresenter<MovieView>() {

  private val datasource =
      Network
          .providesHttpAdapter()
          .create(MovieDatasource::class.java)

  private val factory = MovieFactory(datasource)
  private val repository = MovieRepository(factory)
  private val usecase = MovieUsecase(repository)

  fun discoverMovie() {
    getView().onShowLoading()
    usecase.execute(MovieUsecaseObserver(), Any())
  }

  inner class MovieUsecaseObserver : DefaultObserver<MovieEntity>() {

    override fun onSuccess(entity: MovieEntity) {
      getView().onHideLoading()
      getView().onSuccess(entity.results)
    }

    override fun onError(error: Throwable) {
      getView().onHideLoading()
      getView().onError(error.response().statusMessage)
    }
  }
}