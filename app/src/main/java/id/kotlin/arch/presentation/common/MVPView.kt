package id.kotlin.arch.presentation.common

interface MVPView {

  fun onShowLoading()
  fun onHideLoading()
}