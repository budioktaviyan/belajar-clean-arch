package id.kotlin.arch.presentation.movie

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import id.kotlin.arch.domain.movie.Result

class MovieActivity : AppCompatActivity(), MovieView {

  companion object {
    val TAG: String = MovieActivity::class.java.simpleName
  }

  private val presenter = MoviePresenter()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    presenter.onAttach(this)
    presenter.discoverMovie()
  }

  override fun onDestroy() {
    super.onDestroy()
    presenter.onDetach()
  }

  override fun onShowLoading() {
    Log.d(TAG, "onShowLoading")
  }

  override fun onHideLoading() {
    Log.d(TAG, "onHideLoading")
  }

  override fun onSuccess(entity: List<Result>) {
    Log.d(TAG, "${entity.size}")
  }

  override fun onError(error: String) {
    Log.e(TAG, error)
  }
}